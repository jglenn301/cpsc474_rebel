import copy

import cfr

class OneCardPoker:
    """ A one-card poker game with a definable number of cards in
        a definable number of suits.  Each player has an unlimited stack.
        First bet size is 1; the other player may then fold, call, or reraise
        (by an additional 1).  On a reraise, the process
        continues, alternating between players until one player folds
        or calls, or the maximum pot size is reached (a reraise is not allowed
        if it would exceed the maximum pot size or if calling it would
        exceed the maximum pot size; an odd maximum pot size is then equivalent
        to a maximum one lower.  The maximum pot does not include the ante,
        which is 1 from each player.
    """
    def __init__(self, suits=4, cards=13, max_pot=2):
        self._suits = suits
        self._card = cards
        self._max_pot = max_pot
        self._cards = [(rank, suit) for rank in range(cards) for suit in range(suits)]


    def is_terminal(self, bets):
        """ Determines if the given sequence of bets is terminal.

            bets -- an iterable over (0, 1, 2)
        """
        return ((len(bets) > 1 and bets[-1] == 0)
                or (len(bets) > 1 and bets[-1] == 1 and bets[-2] != 0)
                or sum(bets) >= self._max_pot)


    def deck_size(self):
        """ Returns the size of the deck in this game. """
        return len(self._cards)


    def deal(self, p1_card_index, p2_card_index):
        return (p1_card_index, p2_card_index)


    def value(self, deal, bets):
        """ Returns the value of the given state for player 0.

            deal -- a tuple of card indices
            bets -- a tuple giving the sequence of bets
        """
        last_actor = (len(bets) + 1) % 2
        pot = sum(bets)
        ante = 2
        share = [ante / 2 + sum(x for i, x in enumerate(bets) if i % 2 == p) for p in range(2)]
        
        if pot > 0 and bets[-1] == 0:
            # last player folded
            return -share[0] if last_actor == 0 else share[1]
        elif self._cards[deal[0]][0] == self._cards[deal[1]][0]:
            # draw
            return 0
        else:
            return -share[0] if self._cards[deal[0]][0] < self._cards[deal[1]][0] else share[1]
            

    def uniform_belief(self):
        return [[1.0 / self.deck_size() for card in self._cards] for player in range(2)]

    
    def subgame(self, bets, depth):
        """ Returns the subgame of this game rooted at the given public
            belief state.  The PBS is given as the probability distribution
            over the player's cards and the public bet history.

            bets -- a bet history that is not terminal
            depth -- a nonnegative integr for the maximum depth of the subgame
        """
        return OneCardPoker.Node(self, bets, tuple(), depth, sum(bets))


    class UniformSubgamePolicy(cfr.Policy):
        def __init__(self, game, tree):
            self._game = game
            self._tree = tree


        def distribution(self, info):
            bets = info[1:]

            node, node_index = self._tree.find_node(bets)
            return [1.0 / len(node._actions)] * len(node._actions)


        def distribution_by_card(self, info):
            node, _ = self._tree.find_node(info)
            return [[1.0 / len(node._actions)] * len(node._actions)] * self._game.deck_size()
    
    
    class SubgamePolicy(cfr.Policy):
        """ A policy for a depth-limited subgame of this game.  This can be
            viewed as a policy for the Subgame using the cfr.Policy interface,
            or as a policy for the belief-state representation of the
            game using the distribution_by_card method.
        """
        def __init__(self, game, tree, prob):
            """ game -- a OneCardPoker game
                tree -- a tree for the belief-state representation of
                        a depth-limited subgame of that game
                prob -- a 3-D list with the first dimension being the
                        index of a node in the tree, the second being the
                        card held by the actor, and the third being the
                        index of the action as returned by get_actions.
                        The element prob[i][c][j] is then the probability
                        of taking action j when the game in at node i and
                        the actor holds card c.
            """
            self._game = game
            self._tree = tree
            self._prob = copy.deepcopy(prob)


        def distribution(self, info):
            """ Returns the probability distribution over actions
                in the discrete representation of the game this policy
                is defined for.

                info -- an information set in the Subgame this policy is
                        defined for
            """
            # card held by actor is first part of informations set; rest
            # is bets, of which the end is the path through the tree
            card_index = info[0]
            bets = info[1:]

            node, node_index = self._tree.find_node(bets)
            
            return {a: self._prob[node_index][card_index][action_index]
                    for action_index, a in enumerate(node._actions)}


        def distribution_by_card(self, info):
            """ Returns the probability distribution over possible actions
                for each possible card the actor holds.  The distributions
                are returned as an array where the first index gives the
                card index, the second gives the action index, and the
                corresponding element is the probability of selecting that
                action when holding the given card.

                info -- an information set in the belief-state representation
                        of the subgame (a complete sequence of bets)
            """
            _, node_index = self._tree.find_node(info)
            return self._prob[node_index]

        
    class Subgame(cfr.Game):
        """ A depth-limited subgame of one-card poker.
        """
        def __init__(self, game, tree, beliefs, policy):
            """ Creates the subgame of the given game rooted at the given
                public belief state.  The PBS is given as the probability
                over the player's cards and the public bet history.
            
                game -- a game of OneCardPoker
                tree -- a Tree giving the belief-state game tree for the new
                        subgame
                beliefs -- a pair of list-like objects giving the probablity
                           distribution over the cards for the two players
                policy -- a cfr.Policy for this subgame
            """
            self._game = game
            self._tree = tree
            self._initial_beliefs = beliefs
            self._policy = policy
            self._prefix_len = 2 + len(tree._root._bets)
            self._beliefs = root.belief_states(beliefs, policy)


        def initial_state(self):
            """ Returns an initial (post-deal, pre-bet) state of this subgame 
                sampled from its initial public belief state.
            """
            p_card0 = {i: self._initial_beliefs[0][i] for i in range(self._game.deck_size())}
            c0_index = cfr.select(p_card0)
            p_c0 = self._initial_beliefs[0][c0_index]
            deal = [c0_index]
            
            p_card1 = {i: self._initial_beliefs[1][i] / (1.0 - p_c0) for i in range(self._game.deck_size()) if i != c0_index}
            deal.append(cfr.select(p_card1))
            return tuple(deal) + tuple(self._tree._root._bets)

        
        def all_initial(self):
            """ Retuns all possible initial (post-deal, pre-bet) states
                and their probabilities as a list of (state, probability)
                pairs.
            """
            # need to refactor CFR to allow for different probabilities of
            # initial states
            states = []
            for i, c0 in enumerate(self._game._cards):
                if self._initial_beliefs[0][i] != 0.0:
                    for j, c1 in enumerate(self._game._cards):
                        if c1 != c0 and self._initial_beliefs[1][j] != 0.0:
                            states.append(((c0, c1) + tuple(self._tree._root._bets), self._initial_beliefs[0][i] * self._initial_beliefs[1][j] / (1.0 - self._initial_beliefs[1][i])))
            return states


        def is_terminal(self, hist):
            # strip off the bets during this subgame
            path = hist[self._prefix_len:]

            # find the node at the end of that path
            node = self._tree._node_list[self._tree._path_map[path]]

            # state is terminal *in this subgame* if it is a leaf
            return node.is_leaf()


        def payoff(self, hist, i):
            # separate history into deal and bets (incl. those before subgame)
            deal = hist[:2]
            path = hist[2:]
            
            if self._game.is_terminal(hist[2:]):
                # history is a leaf in the subgame b/c it is terminal in
                # the entire game
                p0_payoff = self._game.value(hist[:2], path)
                pi_payoff = p0_payoff if i == 0 else -p0_payoff
                return pi_payoff
            else:
                # TO-DO: put in code that labels non-terminal leaves
                # w/ values obtained from approximator (neural network)
                # using belief state of each node as input
                pass


        def get_actions(self, info):
            """ Returns the list of possible actions in the given state.
                Each action is represented as a tuple.

                info -- an info set that is not terminal in this subgame
            """
            # get path through this subgame
            path = info[self._prefix_len - 1:]

            # node has the actions stored; CFR wants them as tuples
            actions = self._tree._node_list[self._tree._path_map[path]]._actions
            return [(a,) for a in actions]


        def information_set(self, hist, i):
            """ Returns the information set for the given player for the
                given history.

                hist -- a tuple giving the history, starting with the deal
                i -- 0 or 1 for which player to return the information set for
            """
            # information set is player's card + all subsequent actions
            info = hist[i:i+1] + hist[2:]
            return info


        def compatible_histories(self, history):
            """ Returns all histories corresponding to states that are
                in the same information set as the state for the given
                history.

                history -- a history
            """
            turn = len(history) % 2
            compat = []
            for other_card in range(self._game._cards):
                if other_card != history[turn]:
                    h_prime = list(history)
                    h_prime[1 - turn] = other_card
                    h_prime = tuple(h_prime)
                    compat.append(h_prime)
            return compat

        
    class Node:
        def __init__(self, game, bets, path, depth, pot):
            self._bets = bets
            self._game = game
            self._path = path

            if depth == 0 or game.is_terminal(bets):
                self._actions = []
                self._children = []
            else:
                # the possible actions
                if len(bets) == 0:
                    self._actions = [0, 1] # first action is check or bet
                elif bets == (0,):
                    self._actions = [0, 1] # check or bet after check
                elif pot + 3 > game._max_pot:
                    self._actions = [0, 1] # fold, call if reraise exceeds max pot
                else:
                    self._actions = [0, 1, 2] # fold, call, reraise
    
                # make the children
                self._children = [OneCardPoker.Node(game, bets + (a,), path + (a,), depth - 1, pot + a) for a in self._actions]


        def count_actions(self):
            return len(self._actions)

                
        def collect_nodes(self):
            node_list = []
            node_map = {}
            path_map = {}
            self._collect_nodes(node_list, node_map, path_map)
            return node_list, node_map, path_map


        def _collect_nodes(self, node_list, node_map, path_map):
            node_map[self] = len(node_list)
            path_map[tuple(self._path)] = len(node_list)
            node_list.append(self)

            for child in self._children:
                child._collect_nodes(node_list, node_map, path_map)
                                

        def belief_states(self, pbs, policy):
            states = []
            self._belief_states(pbs, policy, states)
            return states


        def _belief_states(self, pbs, policy, states):
            node_index = len(states)
            actor = len(self._bets) % 2
            
            states.append(pbs)

            for action_index, child in enumerate(self._children):
                # update pbs for actor's cards
                # pbs gives us p(card)
                # policy gives us p(action | card)
                # need p(card | action) = p(action | card) * p(card) / p(action)
                p_action = sum(policy.distribution_by_card(self._bets)[card_index][action_index] * p_card for card_index, p_card in enumerate(pbs[actor]))
                new_pbs = [None] * 2
                new_pbs[actor] = [policy.distribution_by_card(self._bets)[card_index][action_index] * p_card / p_action if p_action > 0.0 else 0.0
                                  for card_index, p_card in enumerate(pbs[actor])]

                # update pbs for other player's cards
                other = 1 - actor
                new_pbs[other] = [sum(new_pbs[actor][actor_card] * pbs[other][other_card] / (1.0 - pbs[other][actor_card])
                                      for actor_card in range(self._game.deck_size())
                                      if actor_card != other_card)
                                  for other_card in range(self._game.deck_size())]
                
                child._belief_states(new_pbs, policy, states)


        def expected_value(self, pbs, policy, beliefs, nodes):
            # for each player and infoset for that player, a list of
            # (value, probability) pairs over all states in that infoset
            infoset_ev = [{}, {}]
            
            #  accumulate the value of this node over all possible deals
            value = 0.0
            for i in range(self._game.deck_size()):
                for j in range(self._game.deck_size()):
                    if i != j:
                        deal = self._game.deal(i, j)
                        p_deal = pbs[0][i] * pbs[1][j] / (1.0 - pbs[1][i])
                        deal_value = self._expected_value(deal, policy, beliefs, nodes)
                        value += p_deal * deal_value
                        for p in range(2):
                            if deal[p] not in infoset_ev[p]:
                                infoset_ev[p][deal[p]] = []
                            infoset_ev[p][deal[p]].append((deal_value, p_deal))

            print(infoset_ev)
            # for each player and all infosets, determine the value of the
            # infoset from the values of the states
            for p in range(2):
                for info in infoset_ev[p]:
                    state_values = infoset_ev[p][info]
                    p_info = sum(prob for value, prob in state_values)
                    v_info = sum(value * prob / p_info for value, prob in state_values)
                    infoset_ev[p][info] = v_info
            
            return value, infoset_ev
                

        def _expected_value(self, deal, policy, beliefs, nodes):
            if self._game.is_terminal(self._bets):
                #print("TERMINAL VALUE:", deal, self._bets, self._game.value(deal, self._bets))
                return self._game.value(deal, self._bets)
            elif len(self._children) == 0:
                # would use neural network here
                return 0.0
            else:
                node_index = nodes[self]
                actor = len(self._bets) % 2
                value = 0.0
                for action_index, child in enumerate(self._children):
                    p_child = policy.distribution_by_card(self._bets)[deal[actor]][action_index]
                    v_child = child._expected_value(deal, policy, beliefs, nodes)
                    value += p_child * v_child
                #print("VALUE:", deal, self._bets, value)
                return value

            
        def is_leaf(self):
            return len(self._children) == 0

            
        def __repr__(self):
            if len(self._actions) == 0:
                return str(self._bets)
            else:
                return "(" + str(self._bets) + " " + " ".join([str(child) for child in self._children]) + ")"


    class Tree:
        """ The game tree for the belief-state representation of a
            subgame of this game.
        """
        def __init__(self, game, root):
            """ game -- a OneCardPoker game
                root -- the root node of a tree for a subgame of that game
            """
            self._game = game
            self._root = root
            self._node_list, self._node_map, self._path_map = root.collect_nodes()
            self._prior_bets = len(self._root._bets)


        def find_node(self, bets):
            """ Returns the Node and its unique index for the given
                complete sequence of bets.

                bets -- a list giving the complete sequence of bets
            """
            path = bets[self._prior_bets:]
            index = self._path_map[tuple(path)]
            node = self._node_list[index]
            return node, index

        
        def uniform_policy(self):
            return OneCardPoker.UniformSubgamePolicy(self._game, self)
        
        
        def players_to_policy(self, players):
            """ Returns a policy corresponding to the given cfr.Players.

                players -- a list of 2 cfr.Players so that players[i] plays
                           this game as player i
            """
            policy = []

            def make_policy(node):
                node_index = len(policy)
                actor = len(node._bets) % 2

                node_policy = []
                for card_index, card in enumerate(node._game._cards): 
                    # infoset is rank of card current player and bet sequence
                    info = (card_index,) + tuple(node._bets)
                    p_actions = players[actor].get_average(info)
                    dist = [p_actions[(action,)] for action in node._actions]
                    node_policy.append(dist)
                policy.append(node_policy)

                for child in node._children:
                    make_policy(child)

            # traverse the tree to build the policy
            make_policy(self._root)
            return OneCardPoker.SubgamePolicy(self._game, self, policy)
        

if __name__ == "__main__":
    game = OneCardPoker(1, 4, 4)
    root = game.subgame(tuple(), 2)

    # ([] ([0] [0, 0] [0, 1]) ([1] [1, 0] [1, 1] [1, 2]))
    print(root)
    
    tree = OneCardPoker.Tree(game, root)
    node_list, node_map, path_map = root.collect_nodes()

    # LIST: [([] ([0] [0, 0] [0, 1]) ([1] [1, 0] [1, 1] [1, 2])), ([0] [0, 0] [0, 1]), [0, 0], [0, 1], ([1] [1, 0] [1, 1] [1, 2]), [1, 0], [1, 1], [1, 2]]
    print("LIST:", node_list)

    # NODE MAP: {([] ([0] [0, 0] [0, 1]) ([1] [1, 0] [1, 1] [1, 2])): 0, ([0] [0, 0] [0, 1]): 1, [0, 0]: 2, [0, 1]: 3, ([1] [1, 0] [1, 1] [1, 2]): 4, [1, 0]: 5, [1, 1]: 6, [1, 2]: 7}
    print("NODE MAP:", node_map)

    # PATH MAP: {(): 0, (0,): 1, (0, 0): 2, (0, 1): 3, (1,): 4, (1, 0): 5, (1, 1): 6, (1, 2): 7}
    print("PATH MAP:", path_map)
    
    policy = tree.uniform_policy()
    
    pbs = game.uniform_belief()
    beliefs = root.belief_states(pbs, policy)

    # all [[0.25, 0.25, 0.25, 0.25], [0.25, 0.25, 0.25, 0.25]]
    print(beliefs)
    
    # note that order of cards is reversed from notes (T, J, Q, K)
    policy = OneCardPoker.SubgamePolicy(game, tree,
             [[(0.75, 0.25), (1, 0), (5.0/6.0, 1.0/6.0), (0.2, 0.8)],  # root
              [(0.5, 0.5), (1, 0), (1, 0), (0, 1)],                    # [0]
              None,
              None,
              [(0.5, 0.5, 0), (1, 0, 0), (0, 1, 0), (0, 1, 0)],        # [1]
              None,
              None,
              None])
    beliefs = root.belief_states(pbs, policy)

    # [0]: [[0.26946107784431134, 0.35928143712574845, 0.2994011976047904, 0.0718562874251497], [0.24351297405189617, 0.21357285429141712, 0.2335329341317365, 0.30938123752495006]]
    # [0, 0]: [[0.3157894736842104, 0.31578947368421045, 0.26315789473684204, 0.10526315789473682], [0.21403508771929824, 0.375438596491228, 0.4105263157894737, 0.0]]
    # [0, 1]: [[0.20833333333333331, 0.4166666666666665, 0.34722222222222215, 0.027777777777777776], [0.2824074074074074, 0.0, 0.0, 0.7175925925925926]]
    # [1]: [[0.2054794520547945, 0.0, 0.136986301369863, 0.6575342465753424], [0.2648401826484018, 0.3333333333333333, 0.2876712328767123, 0.1141552511415525]]
    # [1, 0]: [[0.14705882352941177, 0.0, 0.14705882352941174, 0.7058823529411764], [0.28431372549019607, 0.7156862745098039, 0.0, 0.0]]
    # [1, 1]: [[0.2564102564102564, 0.0, 0.1282051282051282, 0.6153846153846154], [0.2478632478632479, 0.0, 0.5384615384615385, 0.2136752136752137]]
    # [1,2]: [[0.0, 0.0, 0.0, 0.0], [0.0, 0.0, 0.0, 0.0]]
    print("HAND-CODED POLICY:", beliefs)
    

    ev = root.expected_value(pbs, policy, beliefs, node_map)

    # 0.22361111111111115
    print("EXPECTED VALUE:", ev[0])

    # [{0: -0.7499999999999999, 1: -0.16666666666666666, 2: 0.4444444444444445, 3: 1.3666666666666667}, {1: 0.5, 2: -0.15000000000000002, 3: -0.2777777777777778, 0: 0.8222222222222223}]
    print("EV BY INFOSET:", ev[1])
    
    subgame = OneCardPoker.Subgame(game, tree, pbs, policy)

    # all 0.08333...
    print(subgame.all_initial())

    print(subgame.is_terminal((0, 3, 0)))     # False
    print(subgame.is_terminal((0, 3, 0, 1)))  # True
    
    print("RUNNING CFR")
    
    game = OneCardPoker(1, 4, 2)
    root = game.subgame(tuple(), 3)
    tree = OneCardPoker.Tree(game, root)
    node_list, node_map, path_map = root.collect_nodes()
    subgame = OneCardPoker.Subgame(game, tree, game.uniform_belief(), tree.uniform_policy())
    players = cfr.train(100000, subgame)

    # APPROXIMATELY
    #(0,) {(0,): 0.75, (1,): 0.25}
    #(1,) {(0,): 1.0, (1,): 0.0}
    #(2,) {(0,): 1.0, (1,): 0.0}
    #(3,) {(0,): 0.25, (1,): 0.75}
    #(0, 0, 1) {(0,): 1.0, (1,): 0.0}
    #(1, 0, 1) {(0,): 0.75, (1,): 0.25}
    #(2, 0, 1) {(0,): 0.0, (1,): 1.0}
    #(3, 0, 1) {(0,): 0.0 (1,): 1.0}
    #(0, 0) {(0,): 0.5, (1,): 0.5}
    #(0, 1) {(0,): 1.0, (1,): 1.0}
    #(1, 0) {(0,): 1.0, (1,): 0.0}
    #(1, 1) {(0,): 1-3x, (1,): x}
    #(2, 0) {(0,): 3x, (1,): 1-3x}
    #(2, 1) {(0,): x, (1,): 1-x}
    #(3, 0) {(0,): 0.0, (1,): 1.0}
    #(3, 1) {(0,): 0.0, (1,): 1.0}
    print("EQUILIBRIUM STRATEGY PROFILE")
    for p in players:
        for hist in p._prob_sum:
            print(hist, p.get_average(hist))
    policy = tree.players_to_policy(players)
    pbs = game.uniform_belief()
    print(policy)
    subgame = OneCardPoker.Subgame(game, tree, pbs, policy)
    beliefs = root.belief_states(pbs, policy)

    # approxiately -0.41666666, [{0: -1.0, 1: -2/3, 2: 1/6, 3: 1 1/3},
    #                            {0: 1.0, 1: 0.5, 2: -1/6, 3: -1 1/2}] 
    print("EXPECTED VALUE:", root.expected_value(pbs, policy, beliefs, node_map))
    # approximately -0.4166666
    print("RESULTS OF PLAY:", cfr.play(1000000, subgame, players))
